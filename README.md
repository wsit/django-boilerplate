Django Boilerplate Technical Doc & Flow
===

This documentation will describe technologies and tools uesed to develop this application.
Besides this documentation also provide the procedure for running the application.

##### Language:
    Python (3.6.0)
     
##### Database:
    PostgreSQL (9.5.25)

##### Frameworks:
    Django (3.2.6)
    django rest framework (3.12.4)

##### Other libraries / tools:
    Pillow (8.3.1)
    django-oauth-toolkit (1.5.0)
    django-sendgrid-v5 (1.2.0)
    Others can be found in requirement.txt file


<br>

##### Setup

The first thing to do is to clone the repository:

```sh
$ git clone https://gitlab.com/wsit/django-boilerplate.git
$ cd django-boilerplate
```

Create a virtual environment to install dependencies in and activate it:

```sh
$ virtualenv2 --no-site-packages env
$ source env/bin/activate
```

Then install the dependencies:

```sh
(env)$ pip install -r requirements.txt
```
Note the `(env)` in front of the prompt. This indicates that this terminal
session operates in a virtual environment set up by `virtualenv2`.

Once `pip` has finished downloading the dependencies:
```sh
(env)$ cd project
(env)$ python manage.py migrate
(env)$ python manage.py runserver
```

#### Project Description:

It's Boilerplate with authentication service.



#### Project Structure:

All modules are django app based. Following tree will represent the project structure. <br>
Note: In the tree, root folder files and folders will be labeled. `django_boiler` folder is the project setting folder and
`serviceapp` is the standard django app folder structure.   


```
├── .gitignore (git ignore file)
├── manage.py (project entrypoint)
├── README.md (technical doc & flow file)
├── requirements.txt (project dependencies)
├── django_boiler
│   └── settings.py (project main setting and config)
│   └── urls.py (project main routing)
│   └── wsgi.py (standard for web server config)
│   └── ...
├── serviceapp
│   └── migrations
│      └── 001_initial.py (app migration file)
│      └── ...
│   └── serializers
│      └── user_serializer.py (user sercialzer config)
│      └── ...
│   └── templates (template files)
│      └── ...
│   └── views (app url's functionality)
│      └── ...
│   └── admin.py (app admin config)
│   └── apps.py (app setting file)
│   └── models.py (app models are here)
│   └── urls.py (app routing)
│   └── ...
└── ...
```

## Tests

To run the tests, `cd` into the directory where `manage.py` is:
```sh
(env)$ python manage.py test
